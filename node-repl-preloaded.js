#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const child_process = require('child_process');

function main() {
	let directory = process.cwd();
	let package_json;
	while (true) {
		try {
			package_json = JSON.parse(
				fs.readFileSync(path.join(directory, 'package.json'), {encoding: 'utf8'}),
			);
			break;
		} catch(error) {
			directory = path.dirname(directory);
			// Break if at top directory
			if (directory === path.dirname(directory)) {
				break;
			}
		}
	}

	if (!package_json) {
		console.error('package.json invalid or not found');
		process.exit(1);
	}

	const {node_repl} = package_json;
	if (!node_repl) {
		console.error('No `node_repl` property in `package.json`')
		process.exit(2);
	}
	const modules = Object.keys(node_repl?.packages || []).map(
		key => `const ${key} = require('${node_repl?.packages[key]}')`,
	).join(';\n')
	child_process.spawn('node', ['--interactive', '--eval', modules], {stdio: 'inherit'});
};

main();
