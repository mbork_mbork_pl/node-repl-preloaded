# Node.js REPL with preloaded packages

A script to run Node.js REPL with some packages preloaded.  This can be useful when you want to experiment with some pieces of code, but require a utility package like Lodash, Ramda, Luxon, or your own module for database connection.

## Usage
Add a property `node_repl` to your `package.json` and an object `packages` under it.  Every entry in `node_repl.packages` is a package to preload: the key is the name under which you want it to be loaded in the REPL and the value is the actual name of the package.  For example, you may want your `package.json` to look like this:
```
{
  // ...
  "node_repl": {
    "packages": {
	  "_": "lodash"
	}
  }
}
```

Install `node-repl-preloaded` as a dev dependency, for example using `npm install --save-dev node-repl-preloaded`.

Start it with `npx node-repl-preloaded`.

## Limitations
This is a very early version (and my first package on npm), so I used it as a playground to learn the publishing process.  As of now, it only supports CJS style packages (that is, it uses `require`, not `import`).
